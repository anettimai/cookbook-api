<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_books', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recipe_book_id');
            $table->unsignedInteger('collection_id');
            $table->foreign('recipe_book_id')->references('id')->on('recipe_books');
            $table->foreign('collection_id')->references('id')->on('collections');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_books');
    }
}
