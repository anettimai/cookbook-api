xs<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRecipesTable extends Migration
{

    public function up()
    {
        Schema::table('recipes', function($table) {
            $table->string('cover_photo')->nullable()->unique()->after('serve_amount');
        });
    }

    public function down()
    {
        Schema::table('recipes', function($table) {
            $table->dropColumn('cover_photo');
        });
    }
}
