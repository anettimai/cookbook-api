<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddFcmToUserTable extends Migration
{

    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('fcm_key')->nullable()->unique()->after('api_key');
        });
    }

    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('fcm_key');
        });
    }
}
