<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateRecipeBooksTable extends Migration
{

    public function up()
    {
        Schema::table('recipe_books', function($table) {
            $table->string('cover_photo')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('recipes', function($table) {
            $table->dropColumn('cover_photo');
        });
    }
}
