<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateIngredientsTableTable extends Migration
{

    public function up()
    {
        Schema::table('recipe_ingredients', function($table) {
            $table->decimal('quantity')->nullable()->change();
            $table->string('measurement')->nullable()->change();
        });
    }

    public function down()
    {
    }
}
