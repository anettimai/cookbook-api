<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDropUniqueFcmTable extends Migration
{
    public function up()
    {
        Schema::table('users', function($table) {
            $table->dropUnique('users_fcm_key_unique');
        });
    }

    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('fcm_key');
        });
    }
}
