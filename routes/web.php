<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api', 'middleware' => ['auth']], function () use ($router) {

  $router->post('login/','UsersController@authenticate');
  $router->post('register/', 'UsersController@register');

  // User routes.
  $router->group(['prefix' => 'users'], function () use ($router) {
    $router->get('/{id}', 'UsersController@get');
    $router->delete('/{id}', 'UsersController@remove');
    $router->put('/{id}', 'UsersController@put');
    $router->get('/recipe-book/{id}', 'UsersController@getRecipeBooks');
  });

  // Recipe book routes.
  $router->group(['prefix' => 'recipe-books'], function () use ($router) {
    $router->get('/', 'RecipeBooksController@all');
    $router->get('/{id}/recipes', 'RecipeBooksController@getRecipes');
    $router->get('/{id}/users', 'RecipeBooksController@getUsers');
    $router->get('/{id}', 'RecipeBooksController@get');
    $router->post('/', 'RecipeBooksController@add');
    $router->post('/{id}/share', 'RecipeBooksController@share');
    $router->put('/{id}', 'RecipeBooksController@put');
    $router->delete('/{id}', 'RecipeBooksController@remove');
  });

  // Recipe routes.
  $router->group(['prefix' => 'recipes'], function () use ($router) {
    $router->get('/', 'RecipesController@all');
    $router->get('/{id}', 'RecipesController@get');
    $router->post('/', 'RecipesController@add');
    $router->put('/{id}', 'RecipesController@put');
    $router->delete('/{id}', 'RecipesController@remove');
  });

  // Ingredient routes.
  $router->group(['prefix' => 'ingredients'], function () use ($router) {
    $router->get('/', 'IngredientsController@all');
    $router->get('/{id}', 'IngredientsController@get');
    $router->post('/', 'IngredientsController@add');
    $router->put('/{id}', 'IngredientsController@put');
    $router->delete('/{id}', 'IngredientsController@remove');
  });

  // Allergen routes.
  $router->group(['prefix' => 'allergens'], function () use ($router) {
    $router->get('/', 'AllergensController@all');
    $router->get('/{id}', 'AllergensController@get');
    $router->post('/', 'AllergensController@add');
    $router->put('/{id}', 'AllergensController@put');
    $router->delete('/{id}', 'AllergensController@remove');
  });

  // Collection routes.
  $router->group(['prefix' => 'collections'], function () use ($router) {
    $router->get('/', 'CollectionsController@all');
    $router->get('/{id}', 'CollectionsController@get');
    $router->post('/', 'CollectionsController@add');
    $router->put('/{id}', 'CollectionsController@put');
    $router->delete('/{id}', 'CollectionsController@remove');
  });
});
