<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    /**
     * Create a new middleware instance.
     *
     * @return void
     */
    public function __construct()
    {}

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // Add proper headers to requests.
        $headers = [
            'Access-Control-Allow-Origin'      => 'origin',
            'Access-Control-Allow-Methods'     => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Max-Age'           => '86400',
            'Access-Control-Allow-Headers'     => '*',
            'Content-Type'                     => 'application/json'
        ];
        
        if ($request->isMethod('OPTIONS'))
        {
            $response = response()->json('{"method":"OPTIONS"}', 200, $headers);
        } else {
            $response = $next($request);
            foreach($headers as $key => $value)
            {
                $response->header($key, $value);
            }
        }
    
        return $response;
    }
}
