<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Recipe;
use App\Http\Models\RecipeIngredient;
use App\Http\Models\RecipeStep;
use App\Http\Models\Ingredient;
use App\Http\Controllers\Controller;

class RecipesController extends Controller {
    
    /**
     * Add item to model.
     *
     * @param Request $request
     * @return Response
     */
    public function add(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $title = $request->input('title');
        $notes = $request->input('notes');
        $prep_time = $request->input('prep_time');

        $cooking_time = $request->input('cooking_time');
        $serve_amount = $request->input('servings');
        $book_id = $request->input('book_id');
        $image = $this->saveImage($request->input('photo'));
        
        $recipe = Recipe::create([
            'title' => $title,
            'notes' => $notes,
            'recipe_book_id' => $book_id,
            'prep_time' => $prep_time,
            'cook_time' => $cooking_time,
            'serve_amount' => $serve_amount,
            'cover_photo' => $image
        ]);

        if ($recipe) {
            $this->saveIngredients($request->input('ingredients'), $recipe->id);
            $this->saveSteps($request->input('steps'), $recipe->id);
            return response()->json([
                'success' => true
            ], 200);
        }
        else {
            return response()->json([
                'success' => false
            ], 401);
        }
    }

    /**
     * Save the recipe's ingredients.
     *
     * @param array $ingredients
     * @param int $recipe_id
     * @return void
     */
    protected function saveIngredients($ingredients, $recipe_id)
    {
        foreach ($ingredients as $key => $ingredient) {
            // Check if ingredient exists.
            $saved_ingredient = Ingredient::where('name', $ingredient['ingredient'])->first();

            // Check if the result has an ingredient.
            if ($saved_ingredient && !$saved_ingredient->exists()) {
                $saved_ingredient = null;
            }

            // If it does not, save it.
            if (!$saved_ingredient) {
                $saved_ingredient = Ingredient::create([
                    'name' => $ingredient['ingredient']
                ]);
            }
            
            RecipeIngredient::create([
                'recipe_id' => $recipe_id,
                'ingredient_id' => $saved_ingredient->id,
                'quantity' => $ingredient['quantity'],
                'measurement' => $ingredient['unit']
            ]);
        }
    }

    /**
     * Save the recipe's steps.
     *
     * @param array $steps
     * @param int $recipe_id
     * @return void
     */
    protected function saveSteps($steps, $recipe_id)
    {
        foreach ($steps as $key => $step) {
            RecipeStep::create([
                'recipe_id' => $recipe_id,
                'instruction' => $step,
                'step_number' => $key,
                'image' => null,
            ]);
        }
    }

    /**
     * Save the recipe's cover picture.
     *
     * @param [string] $base64
     * @return void
     */
    public function saveImage($base64) 
    {
        $image_url = "recipe-".time().".png";
        $path = public_path() . '/img/recipes/' . $image_url;
        $image = base64_decode($base64);

        if (env('APP_ENV') == 'local') {
            $success = file_put_contents($path, $image);
    
            if ($success) {
                return $image_url;
            }
        } 
        else if (env('APP_ENV') == 'production') {
            $s3 = app()->make('aws')->createClient('s3');

            try {
                $bucket = env('S3_BUCKET')?: die('No "S3_BUCKET" config var in found in env!');
                $upload = $s3->putObject([
                    'Bucket' => $bucket,
                    'Key'    => $image_url,
                    'Body'   => $image,
                    'ACL'    => 'public-read',
                ]);

                return $upload->get('ObjectURL');
            } catch (Aws\S3\Exception\S3Exception $e) {
                echo "There was an error uploading the file.\n";
            }
        }
    }

    /**
     * Get a recipe.
     *
     * @param Request $request
     * @return Response
     */
    public function get(Request $request)
    {
        try {
            if ($request->route('id')) {
                $recipe = Recipe::with(['ingredients.ingredient' => function ($query) {
                    $query->select('id', 'name');
                },'steps'])->where('id', $request->route('id'))->first();
                return response()->json(['recipe' => $recipe], 200);
            } else {
                return response()->json(['status' => 'fail'], 401);
            }
        } catch(Exception $e) {
            return response()->json(['status' => 'fail'], 401);
        }
    }
}
