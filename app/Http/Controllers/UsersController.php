<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Models\User;
use App\Http\Models\RecipeBookUser;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class UsersController extends Controller {
    
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Create a new token.
     * 
     * @param \App\User $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
        
        return JWT::encode($payload, env('JWT_SECRET'));
    } 

    /**
     * Register new user
     *
     * @param $request Request
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'name',
            'api_key'
        ]);
 
        $hasher = app()->make('hash');
        $email = $request->input('email');
        $password = $hasher->make($request->input('password'));
        $name = $request->input('name');
        $fcm = $request->header('fcm-key');
        $user = User::create([
            'email' => $email,
            'password' => $password,
            'name' => $name,
            'fcm_key' => $fcm
        ]);

        $user_token = $this->jwt($user);
        User::where('id', $user->id)->update(['api_key' => $user_token]);
 
        return response()->json([
            'success' => true,
            'token' => $user_token,
            'user' => $user
        ], 200);
    }

    /**
    * Display a listing of the resource.
    *
    * @param Request $request
    * @return \Illuminate\Http\Response
    */
   public function authenticate(Request $request)
   {
       // Add user input validations.
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
 
        // Attempt to get the user by the request e-mail.
        $user = User::where('email', $request->input('email'))->first();

        // If the user exists, verify the password as well.
        if ($user && Hash::check($request->input('password'), $user->password)){
            // Save the user token and return it back to the requester.
            $user_token = $this->jwt($user);
            $fcm = $request->header('fcm-key');
            User::where('email', $request->input('email'))->update([
                'api_key' => $user_token,
                'fcm_key' => $fcm
                ]);
            return response()->json([
                'success' => true,
                'token' => $user_token,
                'user' => $user
            ], 200);
        } else {
            return response()->json(['status' => 'fail'], 200);
        }
    }

    /**
     * Get a single user by ID.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request) {
        try {
            $user = User::findOrFail($request->route('id'));
            return response()->json(['user' => $user], 200);
        } catch(Exception $e) {
            return response()->json(['status' => 'fail'], 401);
        }
    }

    /**
     * Get the user's recipe books.
     *
     * @param Request $request
     * @return void
     */
    public function getRecipeBooks(Request $request)
    {
        try {
            $user = User::findOrFail($request->route('id'));
            $books = $user->recipeBooks()->get();
            return response()->json(['recipeBooks' => $books], 200);
        } catch(Exception $e) {
            return response()->json(['status' => 'fail'], 401);
        }
    }
}