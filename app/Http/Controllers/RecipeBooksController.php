<?php namespace App\Http\Controllers;

use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Models\Recipe;
use App\Http\Models\RecipeBook;
use App\Http\Models\RecipeBookUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Models\User;
use Aws\Laravel\AwsServiceProvider;

class RecipeBooksController extends Controller {

    /**
     * Add item to model.
     *
     * @param Request $request
     * @return Response
     */
    public function add(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);
 
        $title = $request->input('title');
        $description = $request->input('description');
        $image = $this->saveImage($request->input('photo'));
        $user_id = $request->input('user');
        
        $recipeBook = RecipeBook::create([
            'title' => $title,
            'description' => $description,
            'user_id' => $user_id,
            'cover_photo' => $image
        ]);

        if ($recipeBook) {
            $recipeBookUser = RecipeBookUser::create([
                'user_id' => $user_id,
                'recipe_book_id' => $recipeBook->id 
             ]);
      
            if ($recipeBookUser) {
                return response()->json([
                     'success' => true
                ], 200);
            }
            else {
                return response()->json([
                     'success' => false
                ], 401);
            }
        }
    }

    /**
     * Save the recipe's cover picture.
     *
     * @param string $base64
     * @return Response
     */
    public function saveImage($base64) 
    {
        $image_url = "recipe-book-".time().".png";
        $image = base64_decode($base64);
        $path = public_path() . '/img/recipe-books/' . $image_url;

        if (env('APP_ENV') == 'local') {
            $success = file_put_contents($path, $image);
    
            if ($success) {
                return $image_url;
            }
        } 
        else if (env('APP_ENV') == 'production') {
            $s3 = app()->make('aws')->createClient('s3');

            try {
                $bucket = env('S3_BUCKET')?: die('No "S3_BUCKET" config var in found in env!');
                $upload = $s3->putObject([
                    'Bucket' => $bucket,
                    'Key'    => $image_url,
                    'Body'   => $image,
                    'ACL'    => 'public-read',
                ]);

                return $upload->get('ObjectURL');
            } catch (Aws\S3\Exception\S3Exception $e) {
                echo "There was an error uploading the file.\n";
            }
        }
         
    }

    /**
     * Get a recipeBook.
     *
     * @param Request $request
     * @return Response
     */
    public function get(Request $request)
    {
        try {
            $recipeBook = RecipeBook::findOrFail($request->route('id'))->get();
            return response()->json(['recipeBook' => $recipeBook], 200);
        } catch(Exception $e) {
            return response()->json(['status' => 'fail'], 401);
        }
    }

    /**
     * Get the recipes.
     *
     * @param Request $request
     * @return Response
     */
    public function getRecipes(Request $request)
    {
        try {
            $recipes = Recipe::with(['ingredients.ingredient' => function ($query) {
                $query->select('id', 'name');
            },'steps'])
            ->where('recipe_book_id', $request->route('id'))
            ->get();
            $recipe_book_title = RecipeBook::where('id', $request->route('id'))->pluck('title')->toArray();
            return response()->json(['recipes' => $recipes, 'book_title' => $recipe_book_title], 200);
        } catch(Exception $e) {
            return response()->json(['status' => 'fail'], 401);
        }
    }

    /**
     * Get the recipe book's users.
     *
     * @param Request $request
     * @return Response
     */
    public function getUsers(Request $request)
    {
        $user_id = Auth::id();
        $id = $request->route('id');
        try {
            $users = RecipeBook::findOrFail($id)->users()->whereNotIn('user_id', [$user_id])->get();
            return response()->json(['users' => $users], 200);
        } catch(Exception $e) {
            return response()->json(['status' => 'fail'], 401);
        }
    }

    /**
     * Share a Recipe Book with a user and send push notification data.
     *
     * @param Request $request
     * @return void
     */
    public function share(Request $request) {
        $email = $request->get('email');
        $user = User::where('email', $request->get('email'))->first();

        if ($user) {
            $recipe_book_user = RecipeBookUser::where([
                'user_id' => $user->id,
                'recipe_book_id' => $request->route('id')
            ])->first();

            if (!$recipe_book_user) {
                $recipe_book_user = RecipeBookUser::create([
                    'user_id' => $user->id,
                    'recipe_book_id' => $request->route('id')
                ]);
    
                $token = $user->fcm_key;
                if ($token) {
                    return $this->sendInvitation($token);
                } 
                else if ($recipe_book_user) {
                    return response()->json(['success' => true], 200);
                } else {
                    return response()->json(['success' => false], 401);
                }
            } else {
                $owner = RecipeBook::findOrFail($request->route('id'))->user()->first();
                if ($recipe_book_user->user == $owner) {
                    return response()->json([
                        'success' => false, 
                        'message' => 'The owner of this Recipe Book is ' . $recipe_book_user->user->name . '.'
                    ], 200);
                } else{
                    return response()->json([
                        'success' => false, 
                        'message' => 'This Recipe Book is already shared with ' . $recipe_book_user->user->name . '.'
                    ], 200);
                }
            }
        } else {
            return response()->json([
                'success' => false, 
                'message' => 'There is no user registered with the e-mail address ' . $email . '.'
            ], 200);
        }
    }

    /**
     * Send push notification data.
     *
     * @param string $token
     * @return void
     */
    public function sendInvitation($token)
    {
        $notification = [
            'title' =>'CookBook',
            'body' => 'A new Recipe Book has been shared with you!'
        ];
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = [
            'to' => $token,
            'notification' => $notification,
            'priority' => 'high'
        ];

        $headers = [
            'Authorization: key=' . env('FIREBASE_ACCESS_KEY'),
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);           
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        return $result;
    }

    /**
     * Delete the recipe book by ID.
     *
     * @param Request $request
     * @return void
     */
    public function remove(Request $request)
    {
        RecipeBook::destroy($request->route('id'));
        return response()->json([
            'success' => true
        ], 200);
    }
}
