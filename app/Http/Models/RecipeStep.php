<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeStep extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recipe_steps';

    protected $fillable = ['recipe_id', 'instruction', 'step_number', 'image'];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function recipe()
    {
        return $this->belongsTo('App\Http\Models\Recipe');
    }

}
