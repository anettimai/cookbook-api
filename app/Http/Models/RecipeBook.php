<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeBook extends Model {

    protected $fillable = ["title", "description", "cover_photo", "user_id"];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships

    /**
     * Get the user that owns the recipe book.
     */
    public function user()
    {
        return $this->belongsTo('App\Http\Models\User');
    }

    /**
     * Get all the users that are shared the recipe book with.
     */
    public function users()
    {
        return $this->belongsToMany('App\Http\Models\User', 'recipe_book_users', 'recipe_book_id', 'user_id');
    }

    public function recipeBooks()
    {
        return $this->belongsToMany('App\Http\Models\RecipeBook', 'recipe_book_users', 'recipe_book_id', 'user_id');
    }

    public function recipeBookUser()
    {
        return $this->hasMany('App\Http\Models\RecipeBookUser', 'recipe_book_id');
    }

    public function recipes()
    {
        return $this->hasMany('App\Http\Models\Recipe');
    }

    // Declare event handlers.
    public static function boot() {
        parent::boot();

        // Delete related items.
        static::deleting(function($book) { 
             $book->recipes()->delete();
             $book->recipeBookUser()->delete();
        });
    }

}
