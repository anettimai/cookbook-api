<?php

namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_key', 'fcm_key'
    ];

    public function collections()
    {
        return $this->hasMany('App\Http\Models\Collection', 'user_id');
    }

    public function recipeBooks()
    {
        return $this->belongsToMany('App\Http\Models\RecipeBook', 'recipe_book_users', 'user_id', 'recipe_book_id');
    }

    public function recipeBook()
    {
        return $this->belongsTo('App\Http\Models\RecipeBook', 'user_id');
    }
}
