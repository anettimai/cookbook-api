<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recipes';

    protected $fillable = ["title", "notes", "prep_time", "cook_time", "serve_amount", "recipe_book_id", "cover_photo"];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function recipeBook()
    {
        return $this->belongsTo('App\Http\Models\RecipeBook');
    }

    public function ingredients()
    {
        return $this->hasMany('App\Http\Models\RecipeIngredient');
    }

    public function steps()
    {
        return $this->hasMany('App\Http\Models\RecipeStep');
    }

    // Declare event handlers.
    public static function boot() {
        parent::boot();

        // Delete related items.
        static::deleting(function($recipe) { 
             $recipe->steps()->delete();
             $recipe->ingredients()->delete();
        });
    }

}
