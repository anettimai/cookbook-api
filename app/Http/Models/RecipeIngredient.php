<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeIngredient extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recipe_ingredients';

    protected $fillable = ['recipe_id', 'ingredient_id', 'quantity', 'measurement'];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function recipe()
    {
        return $this->belongsTo('App\Http\Models\Recipe');
    }
    
    // Relationships
    public function ingredient()
    {
        return $this->belongsTo('App\Http\Models\Ingredient');
    }

    public function ingredients()
    {
        return $this->belongsToMany('App\Http\Models\RecipeIngredient', 'recipe_ingredients', 'ingredient_id', 'recipe_id');
    }

}
