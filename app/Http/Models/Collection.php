<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model {

    protected $fillable = ["name"];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships

}
