<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ingredients';

    protected $fillable = ["name"];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function recipe()
    {
        return $this->belongsTo('App\Http\Models\Recipe');
    }
}
