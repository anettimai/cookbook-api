<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeBookUser extends Model {

    protected $fillable = ["user_id", "recipe_book_id"];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    
    /**
     * Get the user that owns the recipe book.
     */
    public function user()
    {
        return $this->belongsTo('App\Http\Models\User');
    }

    /**
     * Get the user that owns the recipe book.
     */
    public function recipeBook()
    {
        return $this->belongsTo('App\Http\Models\RecipeBook');
    }

}
