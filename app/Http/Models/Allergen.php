<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Allergen extends Model {

    protected $fillable = ["name"];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships

}
